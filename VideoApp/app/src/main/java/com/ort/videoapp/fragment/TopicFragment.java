package com.ort.videoapp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.ort.videoapp.LoaderImageView;
import com.ort.videoapp.R;
import com.ort.videoapp.Video;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;


public class TopicFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ViewFlipper vw;
    LinearLayout llhsv1, llhsv2, llhsv3;

    String[] links = new String[]{"http://onerooftech.com/demoApp/harshal/css/1.mp4",
            "http://onerooftech.com/demoApp/harshal/css/2.mp4", "http://onerooftech.com/demoApp/harshal/css/3.mp4", "http://onerooftech.com/demoApp/harshal/css/4.mp4",
            "http://onerooftech.com/demoApp/harshal/css/5.mp4"};

    public String vidAddress1 = "http://onerooftech.com/demoApp/harshal/css/1.mp4";
    public String vidAddress2 = "http://onerooftech.com/demoApp/harshal/css/2.mp4";
    public String vidAddress3 = "http://onerooftech.com/demoApp/harshal/css/3.mp4";
    public String vidAddress4 = "http://onerooftech.com/demoApp/harshal/css/4.mp4";
    public String vidAddress5 = "http://onerooftech.com/demoApp/harshal/css/5.mp4";
    //final Uri vidUri = Uri.parse("http://onerooftech.com/demoApp/harshal/css/1.mp4");
    public Uri vidUri1,vidUri2,vidUri3,vidUri4,vidUri5;
    public Bitmap img1 = null,img2 = null,img3 = null,img4 = null,img5 = null;
    List<Video> videoList;
    public int[] strImage = new int[]{R.drawable.img1,R.drawable.img2,R.drawable.img3,R.drawable.img4,R.drawable.img5};
    public TopicFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TopicFragment newInstance(List<Video> videoList) {
        TopicFragment fragment = new TopicFragment();
        Bundle args = new Bundle();
        args.putSerializable("videolist", (Serializable) videoList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            videoList = (List<Video>) getArguments().getSerializable("videolist");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_topic, container, false);
        vw = (ViewFlipper) v.findViewById(R.id.viewflipper);

        llhsv1 = (LinearLayout) v.findViewById(R.id.linear1);
        llhsv2 = (LinearLayout) v.findViewById(R.id.linear2);
        llhsv3 = (LinearLayout) v.findViewById(R.id.linear3);

        for (int i = 0; i < 4; i++) {

            LayoutInflater vi = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v1 = vi.inflate(R.layout.image, null);
            // fill in any details dynamically here

            ImageView im = (ImageView) v1.findViewById(R.id.img_ad);
            if (i == 0) {
                im.setImageDrawable(getResources().getDrawable(R.drawable.banner1));
                vw.addView(v1, i);
            } else if (i == 1) {
                im.setImageDrawable(getResources().getDrawable(R.drawable.banner2));
                vw.addView(v1, i);
            } else if (i == 2) {
                im.setImageDrawable(getResources().getDrawable(R.drawable.banner3));
                vw.addView(v1, i);
            } else if (i == 3) {
                im.setImageDrawable(getResources().getDrawable(R.drawable.banner4));
                vw.addView(v1, i);
            }

        }

        //new AsyncTaskRunner().execute();

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(500, 500);
        lp.setMargins(10, 10, 10, 10);
        for(int i = 0;i<videoList.size();i++) {
            final Video v1 = videoList.get(i);
            //v1.getCatId();

            ImageView im = new ImageView(getActivity());
            im.setLayoutParams(lp);
            //im.setGravity(Gravity.CENTER);
            im.setPadding(10,10,10,10);

            im.setImageDrawable(getActivity().getResources().getDrawable(strImage[i]));

            if(v1.getCatId().equals("1"))
            {
                llhsv1.addView(im);
            }
            else if(v1.getCatId().equals("2"))
            {
                llhsv2.addView(im);
            }

            final int finalI = i;
            im.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .addToBackStack(null)
                            .add(R.id.main_activity_content_frame, FullScreenFragment.newInstance(Uri.parse(v1.getVideoLink())))
                            .commit();
                }
            });
        }

        return v;
    }

    public Bitmap getVideoFrame(Uri uri) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(uri.toString(), new HashMap<String, String>());
            return retriever.getFrameAtTime();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
            }
        }
        return null;
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog pd;
        Uri[] vidUri;
        Bitmap[] img;
        @Override
        protected String doInBackground(String... params) {
            //publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                //pbdialog = (ProgressBar) findViewById(R.id.progressBar);
                vidUri = new Uri[5];
                img = new Bitmap[5];

                for (int j = 0; j < links.length; j++) {
                    vidUri[j] = Uri.parse(links[j]);
                }
                /*vidUri1 = Uri.parse(vidAddress1);
                vidUri2 = Uri.parse(vidAddress2);
                vidUri3 = Uri.parse(vidAddress3);
                vidUri4 = Uri.parse(vidAddress4);
                vidUri5 = Uri.parse(vidAddress5);*/

                try {
                    Log.w("start converting","ssf");
                    //pbdialog.setProgress(0);
                    for(int j = 0;j<links.length;j++) {
                        img[j] = retriveVideoFrameFromVideo(links[j]);

                        //pbdialog.setProgress(10*j+10);
                    }

                    /*img1 = retriveVideoFrameFromVideo(vidAddress1);
                    //pbdialog.setProgress(20);
                    img2 = retriveVideoFrameFromVideo(vidAddress2);
                    ///pbdialog.setProgress(40);
                    img3 = retriveVideoFrameFromVideo(vidAddress3);
                    //pbdialog.setProgress(60);
                    img4 = retriveVideoFrameFromVideo(vidAddress4);
                    //pbdialog.setProgress(80);
                    img5 = retriveVideoFrameFromVideo(vidAddress5);*/
                    //pbdialog.setProgress(100);

                    //pbdialog.setVisibility(View.GONE);

                    Log.w("end converting","end");

                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            //finalResult.setText(result);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(10, 10, 10, 10);

            for (int i = 0; i < 5; i++) {
                LoaderImageView im = new LoaderImageView(getActivity(),links[i]);
                //LoaderImageView img = new LoaderImageView(getActivity(),links[i]);
                //img.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                im.setLayoutParams(lp);
                //im.setImageDrawable();
                //im.setImageBitmap(img[i]);
                im.setGravity(Gravity.CENTER);
                im.setPadding(10,10,10,10);
                //img.setImageDrawable(links[i]);
                // Adds the view to the layout
                llhsv1.addView(im);

            }
            for (int i = 0; i < 5; i++) {
                //LoaderImageView img = new LoaderImageView(getActivity(),links[i]);
                LoaderImageView im = new LoaderImageView(getActivity(),links[i]);
                //LoaderImageView img = new LoaderImageView(getActivity(),links[i]);
                //img.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                im.setLayoutParams(lp);
                im.setGravity(Gravity.CENTER);
                //im.setImageDrawable(getResources().getDrawable(R.mipmap.people));
                //im.setImageBitmap(img[i]);
                im.setPadding(10,10,10,10);
                //img.setImageDrawable(links[i]);
                // Adds the view to the layout
                llhsv2.addView(im);

            }
            for (int i = 0; i < 5; i++) {
                LoaderImageView im = new LoaderImageView(getActivity(),links[i]);
                //LoaderImageView img = new LoaderImageView(getActivity(),links[i]);
                //img.setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                im.setLayoutParams(lp);
                im.setGravity(Gravity.CENTER);
                //img.setImageDrawable(getResources().getDrawable(R.mipmap.people));
                //im.setImageBitmap(img[i]);
                im.setPadding(10,10,10,10);
                //img.setImageDrawable(links[i]);
                // Adds the view to the layout
                llhsv3.addView(im);
            }
            pd.dismiss();

        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            // Things to be done before execution of long running operation. For
            // example showing ProgessDialog
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Loading");
            pd.show();
        }

    }

    public Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable
    {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());
        }
        finally
        {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }


}
