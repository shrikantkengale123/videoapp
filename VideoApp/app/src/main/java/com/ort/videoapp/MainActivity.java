package com.ort.videoapp;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ort.videoapp.fragment.HomeFragment;
import com.ort.videoapp.fragment.InterviewFragment;
import com.ort.videoapp.fragment.TopicFragment;

import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    LinearLayout llHome,llTopic,llInterview,llCampaigns,llProfile;
    View fragmain;

    ProgressBar pbdialog;
    //String vidAddress = "https://archive.org/download/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4";
    public String vidAddress1 = "http://onerooftech.com/demoApp/harshal/css/1.mp4";
    public String vidAddress2 = "http://onerooftech.com/demoApp/harshal/css/2.mp4";
    public String vidAddress3 = "http://onerooftech.com/demoApp/harshal/css/3.mp4";
    public String vidAddress4 = "http://onerooftech.com/demoApp/harshal/css/4.mp4";
    public String vidAddress5 = "http://onerooftech.com/demoApp/harshal/css/5.mp4";
    //final Uri vidUri = Uri.parse("http://onerooftech.com/demoApp/harshal/css/1.mp4");
    public Uri vidUri1,vidUri2,vidUri3,vidUri4,vidUri5;
    public Bitmap img1 = null,img2 = null,img3 = null,img4 = null,img5 = null;

    public String[] links = new String[]{"http://onerooftech.com/demoApp/harshal/css/1.mp4",
            "http://onerooftech.com/demoApp/harshal/css/2.mp4", "http://onerooftech.com/demoApp/harshal/css/3.mp4", "http://onerooftech.com/demoApp/harshal/css/4.mp4",
            "http://onerooftech.com/demoApp/harshal/css/5.mp4"};
    public Uri[] vidUri;
    public Bitmap[] img;
    public List<Video> videoList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        llHome = (LinearLayout)findViewById(R.id.llHome);
        llTopic = (LinearLayout)findViewById(R.id.llTopic);
        llInterview = (LinearLayout)findViewById(R.id.llInterview);
        llCampaigns = (LinearLayout)findViewById(R.id.llCampaigns);
        llProfile = (LinearLayout)findViewById(R.id.llProfile);

        Intent i = getIntent();
        videoList = (List<Video>) i.getSerializableExtra("videolist");

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.main_activity_content_frame, HomeFragment.newInstance(videoList))
                .commit();


        llHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.main_activity_content_frame, HomeFragment.newInstance(videoList))
                        .commit();
                Toast.makeText(MainActivity.this, "Home", Toast.LENGTH_SHORT).show();
            }
        });

        llTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.main_activity_content_frame, TopicFragment.newInstance(videoList))
                        .commit();
                Toast.makeText(MainActivity.this, "Topic", Toast.LENGTH_SHORT).show();
            }
        });

        llInterview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.main_activity_content_frame, InterviewFragment.newInstance())
                        .commit();
                Toast.makeText(MainActivity.this, "Interview", Toast.LENGTH_SHORT).show();
            }
        });

        llCampaigns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Campaign", Toast.LENGTH_SHORT).show();
            }
        });

        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Profile", Toast.LENGTH_SHORT).show();
            }
        });

    }
    public Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable
    {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());
        }
        finally
        {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Inflate menu to add items to action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }


    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog pd;
        @Override
        protected String doInBackground(String... params) {
            //publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                //pbdialog = (ProgressBar) findViewById(R.id.progressBar);


                for (int j = 0; j < links.length; j++) {
                    vidUri[j] = Uri.parse(links[j]);
                }
                vidUri1 = Uri.parse(vidAddress1);
                vidUri2 = Uri.parse(vidAddress2);
                vidUri3 = Uri.parse(vidAddress3);
                vidUri4 = Uri.parse(vidAddress4);
                vidUri5 = Uri.parse(vidAddress5);

                try {
                    Log.w("start converting","ssf");
                    //pbdialog.setProgress(0);
                    for(int j = 0;j<links.length;j++) {
                        img[j] = retriveVideoFrameFromVideo(links[j]);
                        //pbdialog.setProgress(10*j+10);
                    }
                    img1 = retriveVideoFrameFromVideo(vidAddress1);
                    //pbdialog.setProgress(20);
                    img2 = retriveVideoFrameFromVideo(vidAddress2);
                    ///pbdialog.setProgress(40);
                    img3 = retriveVideoFrameFromVideo(vidAddress3);
                    //pbdialog.setProgress(60);
                    img4 = retriveVideoFrameFromVideo(vidAddress4);
                    //pbdialog.setProgress(80);
                    img5 = retriveVideoFrameFromVideo(vidAddress5);
                    //pbdialog.setProgress(100);

                    //pbdialog.setVisibility(View.GONE);

                    Log.w("end converting","end");

                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            //finalResult.setText(result);
            pd.dismiss();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.main_activity_content_frame, HomeFragment.newInstance(videoList))
                    .commit();
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            // Things to be done before execution of long running operation. For
            // example showing ProgessDialog
            pd = new ProgressDialog(MainActivity.this);
            pd.setMessage("Loading");
            pd.show();
        }

    }


}
