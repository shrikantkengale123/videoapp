package com.ort.videoapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ort.videoapp.Item;
import com.ort.videoapp.R;
import com.ort.videoapp.fragment.FullScreenFragment;

import java.util.ArrayList;

/**
 * 
 * @author manish.s
 *
 */
 class CustomGridViewAdapter1 extends ArrayAdapter<Item> {
	Context context;
	int layoutResourceId;
	ArrayList<Item> data = new ArrayList<Item>();

	public CustomGridViewAdapter1(Context context, int layoutResourceId,
								 ArrayList<Item> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			holder.txtCategory = (TextView) row.findViewById(R.id.txt_Category);
			holder.txtPosted = (TextView) row.findViewById(R.id.txt_postdate);
			holder.videoThumb = (ImageView) row.findViewById(R.id.video_thumbnail);
			row.setTag(holder);
		} else {
			holder = (RecordHolder) row.getTag();
		}

		final Item item = data.get(position);
		holder.txtCategory.setText(item.getCategory());
		holder.txtPosted.setText(item.getPosted());
		//holder.videoThumb.setImageBitmap(item.getImage());

		/*Glide
				.with(context)
				.load(item.getUri())
				.into(holder.videoThumb);*/

		Glide.with(context)
				.load(item.getUri())
				.placeholder(R.mipmap.ic_comment_black_36dp)
				.into(holder.videoThumb);

		holder.videoThumb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.w("grid","grid");
				FullScreenFragment.newInstance(item.getUri());
			}
		});

		return row;

	}

	static class RecordHolder {
		TextView txtCategory;
		TextView txtPosted;
		ImageView videoThumb;

	}
}