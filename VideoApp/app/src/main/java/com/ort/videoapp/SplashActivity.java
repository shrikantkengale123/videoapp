package com.ort.videoapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ort.videoapp.app.AppConfig;
import com.ort.videoapp.app.AppController;
import com.ort.videoapp.serviceHandler.JSONParser;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SplashActivity extends AppCompatActivity {
    ProgressBar pbdialog;
    private ProgressDialog pDialog;
    private static final String TAG = SplashActivity.class.getSimpleName();

    private static final String TAG_PRODUCT = "product";
    private static final String TAG_SUCCESS = "success";

    private static final String TAG_VID = "v_id";
    private static final String TAG_VNAME = "v_name";
    private static final String TAG_VIMAGE = "v_image";
    private static final String TAG_VLINK = "v_link";
    private static final String TAG_VCID = "v_cid";
    private static final String TAG_VRATE = "v_rate";
    private static final String TAG_VDATE = "v_videodate";
    private static final String TAG_CATNAME = "c_category_name";
    JSONArray prods;
    private List<Video> videoList = new ArrayList<>();
    int[] iVid;
    String[] strVName,strCid,strCatname,strLink,strRate,strImage,strDate;
    Video v;
    JSONParser jsonParser = new JSONParser();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        pbdialog = (ProgressBar) findViewById(R.id.progressBar);
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        new getVideos().execute();
        //getVideos();
    }

    /**
     * Function to store user in MySQL database will post params(tag, name,
     * email, password) to register url
     * */
    private void getVideos() {
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        pbdialog.setProgress(100);

        pbdialog.setVisibility(View.GONE);

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GETVIDEOS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    int success = jObj.getInt(TAG_SUCCESS);
                    if (success==1) {

                        prods = jObj.getJSONArray(TAG_PRODUCT);

                        iVid = new int[prods.length()];
                        strVName = new String[prods.length()];
                        strCid = new String[prods.length()];
                        strCatname = new String[prods.length()];
                        strLink = new String[prods.length()];
                        strImage = new String[prods.length()];
                        strRate = new String[prods.length()];
                        strDate = new String[prods.length()];

                        for (int i = 0; i < prods.length(); i++) {
                            JSONObject c = prods.getJSONObject(i);

                            // Storing each json item in variable
                            iVid[i] = c.getInt(TAG_VID);
                            strVName[i] = c.getString(TAG_VNAME);
                            strImage[i] = c.getString(TAG_VIMAGE);
                            strCid[i] = c.getString(TAG_VCID);
                            strCatname[i] = c.getString(TAG_CATNAME);
                            strLink[i] = c.getString(TAG_VLINK);
                            strRate[i] = c.getString(TAG_VRATE);
                            strDate[i]= c.getString(TAG_VDATE);

                            v= new Video(iVid[i], strVName[i],strLink[i],strCid[i],strCatname[i],strRate[i],strImage[i],strDate[i]);
                            //Log.w(TAG,iVid[i]+ strVName[i]+strLink[i]+strCid[i]+strCatname[i]+strRate[i]);
                            videoList.add(v);
                        }

                        //Toast.makeText(getApplicationContext(), "User successfully registered. Try login now!", Toast.LENGTH_LONG).show();

                        // Launch login activity
                        Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                        intent.putExtra("videolist", (Serializable) videoList);
                        startActivity(intent);
                        finish();
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


class getVideos extends AsyncTask<String,String,String>{
    int success;
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {

        try {
            // Building Parameters
            JSONObject json = null;

            List<NameValuePair> params1 = new ArrayList<NameValuePair>();

            // getting product details by making HTTP request
            // Note that product details url will use GET request
            json = jsonParser.makeHttpRequest(AppConfig.URL_GETVIDEOS, "GET", params1);

            // check your log for com.ort.kinishah.eduapp.json response
            Log.d("Single Product Details", json.toString());

            // com.ort.kinishah.eduapp.json success tag
            success = json.getInt(TAG_SUCCESS);

            if (success == 1) {
                JSONArray prods = json
                        .getJSONArray(TAG_PRODUCT); // JSON Array

                iVid = new int[prods.length()];
                strVName = new String[prods.length()];
                strCid = new String[prods.length()];
                strCatname = new String[prods.length()];
                strLink = new String[prods.length()];
                strImage = new String[prods.length()];
                strRate = new String[prods.length()];
                strDate= new String[prods.length()];

                for (int i = 0; i < prods.length(); i++) {
                    JSONObject c = prods.getJSONObject(i);

                    // Storing each json item in variable
                    iVid[i] = c.getInt(TAG_VID);
                    strVName[i] = c.getString(TAG_VNAME);
                    strImage[i] = AppConfig.URL_Video+ c.getString(TAG_VIMAGE);
                    strCid[i] = c.getString(TAG_VCID);
                    strCatname[i] = c.getString(TAG_CATNAME);
                    strLink[i] = AppConfig.URL_Video+ c.getString(TAG_VLINK);
                    strRate[i] = c.getString(TAG_VRATE);
                    strDate[i]= c.getString(TAG_VDATE);

                    v= new Video(iVid[i], strVName[i],strLink[i],strCid[i],strCatname[i],strRate[i],strImage[i],strDate[i]);

                    videoList.add(v);
                }

            }
            else {
                // username / password doesn't match
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(String file_url) {
        // dismiss the dialog once got all details

        if(success == 1)
        {
            Intent intent = new Intent(SplashActivity.this,MainActivity.class);
            intent.putExtra("videolist", (Serializable) videoList);
            startActivity(intent);
            finish();
        }
        else if(success == 2)
        {

        }
    }

}



}
