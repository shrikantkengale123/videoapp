package com.ort.videoapp.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ort.videoapp.Item;
import com.ort.videoapp.LoaderImageView;
import com.ort.videoapp.R;
import com.ort.videoapp.Video;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class HotFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    GridView grdHot;
    ArrayList<Item> gridArray = new ArrayList<Item>();
    CustomGridViewAdapter customGridAdapter;

 /*   public String vidAddress1 = "http://onerooftech.com/demoApp/harshal/css/1.mp4";
    public String vidAddress2 = "http://onerooftech.com/demoApp/harshal/css/2.mp4";
    public String vidAddress3 = "http://onerooftech.com/demoApp/harshal/css/3.mp4";
    public String vidAddress4 = "http://onerooftech.com/demoApp/harshal/css/4.mp4";
    public String vidAddress5 = "http://onerooftech.com/demoApp/harshal/css/5.mp4";
    //final Uri vidUri = Uri.parse("http://onerooftech.com/demoApp/harshal/css/1.mp4");
    public Uri vidUri1,vidUri2,vidUri3,vidUri4,vidUri5;
    public Bitmap img1 = null,img2 = null,img3 = null,img4 = null,img5 = null;*/

    public int[] strImage = new int[]{R.drawable.img1,R.drawable.img2,R.drawable.img3,R.drawable.img4,R.drawable.img5};

    public String[] links = new String[]{"http://onerooftech.com/demoApp/harshal/css/1.mp4",
            "http://onerooftech.com/demoApp/harshal/css/2.mp4", "http://onerooftech.com/demoApp/harshal/css/3.mp4", "http://onerooftech.com/demoApp/harshal/css/4.mp4",
            "http://onerooftech.com/demoApp/harshal/css/5.mp4"};
    public Uri[] vidUri;
    public Bitmap[] img;
    List<Video> videoList;

    public HotFragment() {
        // Required empty public constructor
    }

    public static HotFragment newInstance(List<Video> videoList) {
        HotFragment fragment = new HotFragment();
        Bundle args = new Bundle();
        args.putSerializable("videolist", (Serializable) videoList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            videoList = (List<Video>) getArguments().getSerializable("videolist");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_hot, container, false);

        grdHot = (GridView)v.findViewById(R.id.grdHot);

        new AsyncTaskRunner().execute();
        //setRetainInstance(true);
        return v;
    }

    public Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable
    {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());
        }
        finally
        {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public Bitmap getVideoFrame(Uri uri) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(uri.toString(), new HashMap<String, String>());
           // retriever.setDataSource(uri.toString(), new HashMap<String, String>());
           // retriever.setDataSource(uri.toString(), new HashMap<String, String>());
            return retriever.getFrameAtTime();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
            }
        }
        return null;
    }

    public class CustomGridViewAdapter extends ArrayAdapter<Item> {
        Context context;
        int layoutResourceId;
        ArrayList<Item> data = new ArrayList<Item>();

        public CustomGridViewAdapter(Context context, int layoutResourceId,
                                     ArrayList<Item> data) {
            super(context, layoutResourceId, data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            RecordHolder holder = null;

            if (row == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new RecordHolder();
                holder.txtCategory = (TextView) row.findViewById(R.id.txt_Category);
                holder.txtPosted = (TextView) row.findViewById(R.id.txt_postdate);
                holder.videoThumb = (ImageView)row.findViewById(R.id.video_thumbnail);
                row.setTag(holder);
            } else {
                holder = (RecordHolder) row.getTag();
            }

            final Item item = data.get(position);
            holder.txtCategory.setText(item.getCategory());
            String[] strSpldate = item.getPosted().split("-");
            holder.txtPosted.setText(strSpldate[2]+"/"+strSpldate[1]+"/"+strSpldate[0]);

            Glide
				.with(context)
				.load(item.getiImage())
				.into(holder.videoThumb);

            /*URL url = null;
            try {
                url = new URL(item.getStrImage());
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                holder.videoThumb.setImageBitmap(bmp);
            } catch (Exception e) {
                e.printStackTrace();
            }
*/
            //Bitmap bmThumbnail;
           // bmThumbnail = ThumbnailUtils.createVideoThumbnail(item.getUri().toString(), MediaStore.Video.Thumbnails.MICRO_KIND);
            //Log.w("image uri",item.getUri().toString());

            /*try {
                holder.videoThumb.setImageDrawable(item.getStrImage());
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }*/



            /*Glide.with(context)
                    .load(item.getUri())
                    .placeholder(R.mipmap.ic_comment_black_36dp)
                    .into(holder.videoThumb);
*/
            holder.videoThumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.w("grid","grid");
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .addToBackStack(null)
                            .add(R.id.main_activity_content_frame, FullScreenFragment.newInstance(item.getUri()))
                            .commit();
                    //FullScreenFragment.newInstance(item.getUri());
                }
            });

            return row;

        }

         class RecordHolder {
            TextView txtCategory;
            TextView txtPosted;
            ImageView videoThumb;

        }
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog pd;
        @Override
        protected String doInBackground(String... params) {
            //publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                //pbdialog = (ProgressBar) findViewById(R.id.progressBar);
                vidUri = new Uri[videoList.size()];
                img = new Bitmap[videoList.size()];

                for(int i = 0;i<videoList.size();i++) {
                    Video v =videoList.get(i);

                    vidUri[i] = Uri.parse(v.getVideoLink());
                   // Bitmap bmThumbnail;

                    // MICRO_KIND: 96 x 96 thumbnail
                    //img[i] = ThumbnailUtils.createVideoThumbnail(v.getVideoLink(),
                    //        MediaStore.Images.Thumbnails.MICRO_KIND);

                    //img[i] = retriveVideoFrameFromVideo(v.getVideoLink());
                    Log.w("link",v.getImage());
                    //gridArray.add(new Item(vidUri[i],v.getImage(),v.getCatName(),"22/10/2015"));
                    gridArray.add(new Item(vidUri[i],strImage[i],v.getCatName(),v.getDate()));
                }


            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            return resp;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            //finalResult.setText(result);
            pd.dismiss();

            customGridAdapter = new CustomGridViewAdapter(getActivity(), R.layout.row_grid, gridArray);
            grdHot.setAdapter(customGridAdapter);

        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            // Things to be done before execution of long running operation. For
            // example showing ProgessDialog
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Loading");
            pd.show();
        }

    }

}
