package com.ort.videoapp.fragment;

import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.rtoshiro.view.video.FullscreenVideoLayout;
import com.ort.videoapp.R;

import java.io.IOException;

public class FullScreenFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FullscreenVideoLayout videoLayout;

    String imguri;
    public FullScreenFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static FullScreenFragment newInstance(Uri imguri) {
        FullScreenFragment fragment = new FullScreenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, imguri.toString());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            imguri = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_full_screen, container, false);
        videoLayout = (FullscreenVideoLayout) v.findViewById(R.id.videoview);
        videoLayout.setActivity(getActivity());
        videoLayout.setShouldAutoplay(false);

        //Uri videoUri = Uri.parse("http://onerooftech.com/demoApp/harshal/css/1.mp4");
        Uri videoUri = Uri.parse(imguri);
        try {
            videoLayout.setVideoURI(videoUri);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return v;
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        videoLayout.resize();
    }

}
