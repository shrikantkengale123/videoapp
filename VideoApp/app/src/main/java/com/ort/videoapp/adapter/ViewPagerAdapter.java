package com.ort.videoapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ort.videoapp.Video;
import com.ort.videoapp.fragment.HotFragment;
import com.ort.videoapp.fragment.TrendingFragment;

import java.util.List;

/**
 * Created by harsh on 11/02/2016.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
    String mainid;
    List<Video> videoList;

    public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb,List<Video> videoList) {
        super(fm);

        this.videoList = videoList;
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
               // HotFragment hotfrag = new HotFragment();
                return HotFragment.newInstance(videoList);
            case 1:
                //TopicFragment topicfrag = new TopicFragment();
                return TrendingFragment.newInstance();
            case 2:
                //TopicFragment topicfrag1 = new TopicFragment();
                return TrendingFragment.newInstance();
            default:
                return null;
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
