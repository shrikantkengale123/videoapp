package com.ort.videoapp;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * 
 * @author manish.s
 *
 */

public class Item {
	Bitmap image;
	String strCategory;
	String strPosted,strimage;
	Uri uri;
	int iImage;
	
	public Item(Uri uri, Bitmap image, String strCategory, String strPosted) {
		super();
		this.image = image;
		this.uri = uri;
		this.strCategory = strCategory;
		this.strPosted = strPosted;
	}
	public Item(Uri uri, String image, String strCategory, String strPosted) {
		super();
		this.strimage = strimage;
		this.uri = uri;
		this.strCategory = strCategory;
		this.strPosted = strPosted;
	}
	public Item(Uri uri, int iImage, String strCategory, String strPosted) {
		super();
		this.iImage = iImage;
		this.uri = uri;
		this.strCategory = strCategory;
		this.strPosted = strPosted;
	}
	public Bitmap getImage() {
		return image;
	}
	public void setImage(Bitmap image) {
		this.image = image;
	}

	public String getCategory() {
		return strCategory;
	}
	public void setCategory(String strCategory) {
		this.strCategory = strCategory;
	}

	public String getStrImage() {
		return strimage;
	}
	public void setStrImage(String strimage) {
		this.strimage = strimage;
	}

	public String getPosted() {
		return strPosted;
	}
	public void setPosted(String strPosted) {
		this.strPosted = strPosted;
	}

	public Uri getUri() {
		return uri;
	}
	public void setUri(Uri uri) {
		this.uri = uri;
	}

	public int getiImage() {
		return iImage;
	}
	public void setiImage(int iImage) {
		this.iImage = iImage;
	}



}
