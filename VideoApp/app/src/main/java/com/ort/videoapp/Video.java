package com.ort.videoapp;

import android.graphics.Bitmap;
import android.net.Uri;

import java.io.Serializable;

/**
 * 
 * @author manish.s
 *
 */

public class Video implements Serializable {
	int vid;
	String strVideoName,strVideoLink,strCatId,strCatName,strRate,strImage,strDate;

	public Video()
	{

	}
	public Video(int vid,String strVideoName,String strVideoLink,String strCatId,String strCatName,String strRate,String strImage,String strDate){
		super();
		this.vid = vid;
		this.strVideoName = strVideoName;
		this.strVideoLink = strVideoLink;
		this.strImage = strImage;
		this.strCatId = strCatId;
		this.strCatName = strCatName;
		this.strRate = strRate;
		this.strDate = strDate;
	}

	public Integer getVid() {
		return vid;
	}
	public void setVid(int vid) {
		this.vid = vid;
	}

	public String getVideoName() {
		return strVideoName;
	}
	public void setVideoName(String strVideoName) {
		this.strVideoName = strVideoName;
	}

	public String getVideoLink() {
		return strVideoLink;
	}
	public void setVideoLink(String strVideoLink) {
		this.strVideoLink = strVideoLink;
	}

	public String getCatId() {
		return strCatId;
	}
	public void setCatId(String strCatId) {
		this.strCatId = strCatId;
	}

	public String getCatName() {
		return strCatName;
	}
	public void setCatName(String strCatName) {
		this.strCatName = strCatName;
	}

	public String getRate() {
		return strRate;
	}
	public void setRate(String strRate) {
		this.strRate = strRate;
	}

	public String getDate() {
		return strDate;
	}
	public void setDate(String strDate) {
		this.strRate = strDate;
	}

	public String getImage() {
		return strImage;
	}
	public void setImage(String strImage) {
		this.strImage = strImage;
	}
}
